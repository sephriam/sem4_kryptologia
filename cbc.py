T=int("101110011000010101111101",2)
SBoxArray=[int("000",2),int("001",2),int("101",2),int("110",2),int("111",2),int("010",2),int("011",2),int("100",2)]

K1=int("110",2)
K2=int("10",2)
K3=int("11",2)

# print(SBox)

V=int("110011",2)
def SBox(value):
    sb=((value&1)<<2)+(value>>1)
    return SBoxArray[sb]

def EC(value):
    L1=(value & int("111000",2))>>3
    R1=value & int("000111",2)
    #print(bin(R1))
    #print(bin(SBox(R1)))
    L2=R1
    R2=(SBox(R1)^K1)^L1
    #print(bin(L2),bin(R2))
    L3=R2
    R3=(SBox(R2)^K2)^L2
    #print(bin(L3),bin(R3))
    L4=R3
    R4=(SBox(R3)^K3)^L3
    #print(bin(L4),bin(R4))
    #print(bin(value), " ---> ", bin((L4<<3)+R4))
    return ((L4<<3)+R4)

for i in range(4):
    value = T & int("111111000000000000000000",2)
    T <<= 6
    value >>= 18
    value = value^V
    res = EC(value)
    print(bin(value), " ---> ", bin(res))
    V = res